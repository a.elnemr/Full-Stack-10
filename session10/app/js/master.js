"use strict";

const _ = document;

function sum() { // logic
    const n1 = _.getElementById('number1').value,
        n2 = _.getElementById('number2').value
    ;

    // validations
    let error = false;
    // isNaN(n1) return true if NaN , return true if is number
    if (isNaN(n1)) {
        _.getElementById('number1-errors').innerText = 'this not a valid number';
        error = true;
    } else {
        _.getElementById('number1-errors').innerText = '';
    }

    if (isNaN(n2)) {
        _.getElementById('number2-errors').innerText = 'this not a valid number';
        error = true;
    } else {
        _.getElementById('number2-errors').innerText = '';
    }
    // end validations

    if (error) {
        return '';
    }

    return parseFloat(n1) + parseFloat(n2);

    // return !error && parseFloat(n1) + parseFloat(n2);
}


//
// let x = sum() + 2;
// console.log(sum() );

/*function print(message) { // print
    // _.getElementById('result').innerText = message; // print text
    // ES 5
    // _.getElementById('result').innerHTML = '<h1>' + message + '</h1>'; // print html code
    // ES 6
    _.getElementById('result').innerHTML = `<h1>${message}</h1>`; // print html code
}*/


// Events (Actions)
_.addEventListener("DOMContentLoaded", function () {

    const btn = _.getElementById('btn-result');
    // ES 5
    /*btn.addEventListener("click", function() {
        // print(sum());
        _.getElementById('result').innerHTML = `<h1>${sum()}</h1>`; // print html code
    });*/
    // ES 6
    btn.addEventListener("click", btnResult);

    const demo = _.getElementById('demo');
    demo.addEventListener("click", () => alert('demo'));

});

// ES 5
/*function btnClick() {
    _.getElementById('result').innerHTML = `<h1>${sum()}</h1>`; // print html code
}*/

// ES 6
const btnResult = () => {
    _.getElementById('result').innerHTML = `<h1 id="result-value">${sum()}</h1><span>Test</span>`;

    /*const resultValue = _.getElementById('result-value');
    resultValue.addEventListener("click", () => alert('result-value'));*/
};
const resultElm = document.getElementById('result');

resultElm.addEventListener('click', e => {
   // alert('clicked in dom');
   if (e.target.id === 'result-value') {
       console.log(e.target);
   }
});




// ***************************************** //

// Task Solution

/**
 * Event Delegation
 * Capturing and bubbling allow us
    to implement one of most powerful
    event handling patterns called event delegation.
    The idea is that if we have a lot of
    elements handled in a similar way,
    then instead of assigning a handler to each of them –
    we put a single handler on their common ancestor.
    In the handler we get event.target,
    see where the event actually happened and handle it.
 * @ref https://javascript.info/event-delegation
 */
/*_.getElementById('result').addEventListener('click', e => {
    if (e.target && e.target.id === 'result-value') {
        alert('result-value');
        console.log(e.target);
    }
});*/

/**
 * Refacing code (Don't Repeat you-self)
 * Event Delegation
 * we can list this in helpers functions
 * @ref https://javascript.info/event-delegation
 * @param elementId
 * @param callBackFunction
 * @param type
 * @private
 */
/*const _addEvent = (elementId, callBackFunction, type = 'click') => {
    _.addEventListener(type, function (e) {
        if (e.target && e.target.id === elementId) {
            callBackFunction(_.getElementById(e.target.id));
        }
    });
};*/

// how to use
/*_addEvent('result-value', e => {
    alert('Refacing code (Don\'t Repeat you-self)');
    console.log(e);
});*/

// ***************************************** //

/**
 * Mutation observer
 * MutationObserver is a built-in object that observes a
    DOM element and fires a callback in case of
    changes.We’ll first take a look at the syntax,
    and then explore a real-world use case,
    to see where such thing may be useful.
 *
 * @ref https://javascript.info/mutation-observer
 */
/*const observer = new MutationObserver(mutation => {
    const resultValue = _.getElementById("result-value");
    if (resultValue) {
        resultValue.addEventListener("click", () => alert("Mutation observer"));
    }
});*/

/**
 * - Options for the observer
 * childList – changes in the direct children of node,
 * subtree – in all descendants of node,
 * attributes – attributes of node,
 * attributeFilter – an array of attribute names, to observe only selected ones.
 * characterData – whether to observe node.data (text content),
 */
/*observer.observe(_.getElementById('result'), {
    childList: true,
    attributes: true,
    subtree: true
});*/
