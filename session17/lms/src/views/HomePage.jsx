import React, {Component} from 'react';
import '../config';
import TechCard from "../components/TechCard";

class HomePage extends Component {

    state = {
        techs: []
    };

    async componentDidMount() {


        
        let sentData={
            method: 'GET',
            header: {
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Credentials' : 'true'
            }
        };

        const response = await fetch(global.config.api_endpoints.tech, sentData);

        const res = await response.json();
        this.setState({
            techs: res.response.data
        });
    }

    render() {
        return (
            <main role="main">
                <section className="jumbotron text-center">
                    <div className="container">
                        <h1>Album example</h1>
                        <p className="lead text-muted">Something short and leading about the collection below—its
                            contents,
                            the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip
                            over it
                            entirely.</p>
                        <p>
                            <a href="#" className="btn btn-primary my-2">Main call to action</a>
                            <a href="#" className="btn btn-secondary my-2">Secondary action</a>
                        </p>
                    </div>
                </section>
                <div className="album py-5 bg-light">
                    <div className="container">
                        <div className="row">
                            {
                                this.state.techs.length ? (this.state.techs.map(tech => (
                                        <TechCard key={tech.id} tech={tech}/>
                                    )))
                                    : <p>...</p>
                            }
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

export default HomePage;
