import React, {Component} from 'react';
import '../config';
import {Link} from "react-router-dom";
import TechViewTutorialSection from "../components/TechViewTutorialSection";

class TechViewPage extends Component {

    state = {
        tech: {}
    };

    async componentDidMount() {
        const {id} = this.props.match.params;
        const response = await fetch(`${global.config.api_endpoints.tech}/${id}`);
        const res = await response.json();
        this.setState({
            tech: res.response.data
        });
    }

    render() {
        const {tech} = this.state;
        return (
            <main role="main">
                <section className="jumbotron text-center">
                    <div className="container">
                        <div className="card-img">
                            <img src={tech.tech_logo} alt=""/>
                        </div>
                        <h1>{tech.name}</h1>
                        <p className="lead text-muted">{tech.description}</p>
                        <p>
                            <Link to={`/tech/${tech.id}/overview`} className="btn btn-primary my-2">Overview</Link>
                            <Link to={`/tech/${tech.id}/environment-setup`} className="btn btn-secondary my-2">Environment
                                Setup</Link>
                        </p>
                    </div>
                </section>

                <section className="text-center">
                    <div className="container">
                        <ul className="list-group">
                            {
                                tech.tutorial && tech.tutorial.tutorial_sections ?
                                    (tech.tutorial.tutorial_sections.map(section => <TechViewTutorialSection
                                        key={section.id}
                                        section={section}/>))
                                    : <p>Empty</p>
                            }
                        </ul>
                    </div>
                </section>
            </main>
        );
    }
}

export default TechViewPage;
