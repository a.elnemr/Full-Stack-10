import React from 'react';
import '../config'
import {Link} from "react-router-dom";

const TechCard = ({tech}) => (
    <div className="col-md-4">
        <div className="card mb-4 shadow-sm">
            <div className="card-img">
                <img src={tech.tech_logo} alt=""/>
            </div>
            <div className="card-body">
                <h3 className="card-header">{tech.name}</h3>
                <p className="card-text">{tech.description}</p>
                <div className="d-flex justify-content-between align-items-center">
                    <div className="btn-group">
                        <Link className="btn btn-sm btn-outline-secondary" to={`tech/${tech.id}`}>
                            View
                        </Link>
                    </div>
                    <small className="text-muted">9 mins</small>
                </div>
            </div>
        </div>
    </div>
);

export default TechCard;