import React from 'react';
import '../config'
import {Link} from "react-router-dom";

const TechViewTutorialSection = ({section}) => (
    <div className="accordion" id="accordionExample">
        <div className="card">
            <div className="card-header" id="headingOne">
                <h2 className="mb-0">
                    <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne"
                            aria-expanded="true" aria-controls="collapseOne">
                        {section.title}
                    </button>
                </h2>
            </div>

            <div id="collapseOne" className="collapse show" aria-labelledby="headingOne"
                 data-parent="#accordionExample">
                <div className="card-body">
                    <ul className="list-group">
                        {section.lessons && section.lessons.map(lesson => (
                            <li className="list-group-item" key={lesson.id}>{lesson.title}</li>))}
                    </ul>
                </div>
            </div>
        </div>
    </div>
);

export default TechViewTutorialSection;