import React, {Component} from 'react';
import './config';
import Header from "./views/layouts/Header";
import HomePage from "./views/HomePage";
import TechViewPage from "./views/TechViewPage";
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

class App extends Component {

    render() {
        return (
            <div className="App">
                <Router>
                    {/*static header*/}
                    <Header/>
                    {/*dynamic page content*/}
                    <Switch>
                        <Route exact path="/"
                               render={props => <HomePage {...props}/>}/>

                        <Route exact path="/tech/:id"
                               render={props => <TechViewPage {...props} />}/>

                        {/*<Route exact path="/tech/:id">
                            <TechViewPage/>
                        </Route>*/}
                    </Switch>
                    {/*static footer*/}
                </Router>
            </div>
        );
    }
}

export default App;
