console.log("Master Page");

// ES 5
// var age = 15;

// ES 6
/*const age_2 = 20;
const mainColor = '333';
let demo = 50;*/

// value type
// let x = 5;
//
// x = 33;
// const n1 = 5,
//     n2 = 8;
//
// let sum = n1 + n2 + x;
//
//
// console.log('sum:', sum);
// x = 66.12; // float
// sum = n1 + n2 + x;
// console.log('after x sum:', sum);

// x = 2; // valid -> int
// n2 = 9; // invalid

// reference type
const _ = document;
const w = window;
const username = _.getElementById('username'); // string
            //         0          1   2    3
const studentsList = ['Ahmed', 'Ali', 5, 'Sara']; // array

studentsList.push('Mohamed');
// console.table(typeof(studentsList[3]));

console.log(typeof(username.value));

// auto casting to string (if we have a string)
// const inputValue = 3 + username.value;
// to solve string values use
const inputValue = 3 + parseFloat(username.value); // if contain a value not a number return NaN (not a number)


console.log(inputValue);