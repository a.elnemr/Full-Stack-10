(() => {
    "use strict";

    // Functions
    const getAllCounties = (q = '') => {
        const countyList = document.querySelector('#country-list');

        let list = ''; // init string
        // init value ; condition ; increment / decrement value
        for (let counter = 0; counter < data.length; counter++) {
            /*let classList = '';

            if (data[counter].isActive) {
                classList += 'active';
            }
            list += `<li class="${classList}">${data[counter].name}</li>`;*/

            // filter
            if (q) {
                let re = new RegExp(q);
                if (re.test(data[counter].name.toLowerCase())) {
                    list += drawList(counter);
                }
            } else {
                list += drawList(counter)
            }

        }
        countyList.innerHTML = list;
    };

    const drawList = counter => {
        if (data[counter].isActive) {
            document.getElementById('selected-country').innerText = data[counter].name;
        }

        return `
                <li country-id="${data[counter].id}" class="country-item ${data[counter].isActive ? 'active' : ''}">
                    ${data[counter].name}
                </li>
            `;
    };


    /**
     * Events
     */

    // Searching
    const countryInput = document.getElementById('country');
    countryInput.addEventListener('keyup', e => {
        getAllCounties(countryInput.value)
    });

    // change active country
    const countryList = document.getElementById('country-list');
    countryList.addEventListener('click', e => {
        const elm = e.target;
        if (elm && elm.classList.contains('country-item')) {
            const countryId = elm.getAttribute('country-id');

            data.map(item => {
                if (countryId == item.id) {
                    item.isActive = true;
                } else {
                    item.isActive = false;
                }
                return item;
            });

            // update countries list
            getAllCounties();
        }
    });

    // Auto Events
    getAllCounties();
})();