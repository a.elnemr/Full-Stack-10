<?php
ini_set('display_errors', 0);

// define vars
$name = 'ahmed';
//$name = 'ahmed' + "a5";
// auto casting
$age = 25 + 5 * "2a5";
$m = 'age';


// printing string
 echo 'Your name : $name \n and your age: \t $age';
// spacial char
 echo 'i\'m ahmed\\';
 echo "i'm \"ahmed\\";
// var name as var
 echo $$m;

// logical operations
// &&, ||, !

echo !(2 || '') && 0 && 5; // 0 or null or false or '' or ""
/*
echo !(1) && 0 && 5; // 0 or null or false or '' or ""
echo 0 && 0 && 5; // 0 or null or false or '' or ""
echo 0 && 5; // 0 or null or false or '' or ""
echo 0; // 0 or null or false or '' or ""
*/

?>

<h1><?php echo $name; ?></h1>
