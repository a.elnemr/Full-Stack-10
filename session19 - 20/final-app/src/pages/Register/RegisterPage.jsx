import React, {Component} from 'react';
import './RegisterPage.scss'
import '../../config/config';

class RegisterPage extends Component {

    state = {
        user: {
            username: '',
            email: '',
            password: '',
            rePassword: '',
            full_name: '',
            phone: '',
        },
        errors: {},
        requiredInputs: [
            'username',
            'email',
            'password',
            'rePassword',
            'full_name',
            'phone',
        ]
    };

    onChangeHandle = e => {
        const {user} = this.state;
        user[e.target.id] = e.target.value;

        this.setState({
            user
        })
    };

    onSubmitHandle = async e => {
        e.preventDefault();

        // presentation logic
        // validation
        const errors = {};
        let invalid = false;


        if (!this.state.user.username) {
            errors.username = ['username required'];
            invalid = true;
        }
        if (!this.state.user.email) {
            errors.email = ['email required'];
            invalid = true;
        }
        if (!this.state.user.password) {
            errors.password = ['password required'];
            invalid = true;
        }
        if (this.state.user.password) {
            if (this.state.user.password.length < 8) {
                errors.password = ['password should be more than 7 chars'];
                invalid = true;
            }
        }
        if (!this.state.user.rePassword) {
            errors.rePassword = ['rePassword required'];
            invalid = true;
        }
        if (this.state.user.rePassword && this.state.user.password) {
            if (this.state.user.rePassword != this.state.user.password) {
                errors.rePassword = ['password not match'];
                invalid = true;
            }
        }
        if (!this.state.user.full_name) {
            errors.full_name = ['full name required'];
            invalid = true;
        }
        if (!this.state.user.phone) {
            errors.phone = ['phone required'];
            invalid = true;
        }

        if (invalid) {
            this.setState({
                errors
            });
            return;
        }

        // call api
        const options = {
            method: 'POST',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            body: JSON.stringify(this.state.user),
            redirect: 'follow'
        };

        const response = await fetch(global.config.api_endpoints.register, options);
        const res = await response.json();
        if (res.errors) {
            // server errors
            this.setState({
                errors: res.errors
            })
        }
    };

    render() {
        const {errors, user} = this.state;
        return (
            <section className="register-page">
                <section className="card p-5">
                    <form onSubmit={this.onSubmitHandle}>
                        <div className="form-group">
                            <label htmlFor="username">*Username</label>
                            <input type="text"
                                   id="username"
                                   value={user.username}
                                   onChange={this.onChangeHandle}
                                   className={`form-control ${errors.username && 'error'}`}/>
                            {errors.username && errors.username.map(error => (
                                <span className="error-mes">{error}</span>
                            ))}

                        </div>
                        <div className="form-group">
                            <label htmlFor="email">*Email</label>
                            <input type="email"
                                   value={user.email}
                                   onChange={this.onChangeHandle}
                                   id="email"
                                   className={`form-control ${errors.email && 'error'}`}/>

                            {errors.email && errors.email.map(error => (
                                <span className="error-mes">{error}</span>
                            ))}
                        </div>
                        <div className="form-group">
                            <label htmlFor="full_name">*Full Name</label>
                            <input type="text"
                                   value={user.full_name}
                                   onChange={this.onChangeHandle}
                                   id="full_name"
                                   className={`form-control ${errors.full_name && 'error'}`}/>

                            {errors.full_name && errors.full_name.map(error => (
                                <span className="error-mes">{error}</span>
                            ))}
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">*Phone</label>
                            <input type="text"
                                   value={user.phone}
                                   onChange={this.onChangeHandle}
                                   id="phone"
                                   className={`form-control ${errors.phone && 'error'}`}/>
                            {errors.phone && errors.phone.map(error => (
                                <span className="error-mes">{error}</span>
                            ))}
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">*Password</label>
                            <input type="password"
                                   value={user.password}
                                   onChange={this.onChangeHandle}
                                   id="password"
                                   className={`form-control ${errors.password && 'error'}`}/>
                            {errors.password && errors.password.map(error => (
                                <span className="error-mes">{error}</span>
                            ))}
                        </div>
                        <div className="form-group">
                            <label htmlFor="rePassword">*Re-Password</label>
                            <input type="password"
                                   value={user.rePassword}
                                   onChange={this.onChangeHandle}
                                   id="rePassword"
                                   className={`form-control ${errors.rePassword && 'error'}`}/>
                            {errors.rePassword && errors.rePassword.map(error => (
                                <span className="error-mes">{error}</span>
                            ))}
                        </div>

                        <div className="text-center">
                            <button className="btn bg-main-color text-white">Register</button>
                        </div>
                    </form>
                </section>
            </section>
        );
    }

}

export default RegisterPage;