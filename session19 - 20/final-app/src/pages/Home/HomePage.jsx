import React, {Component} from 'react';
import './HomePage.scss'
import TechCard from "../../components/TechCard";
import '../../config/config';

class HomePage extends Component {

    state = {
        technologies: []
    };

    async componentDidMount() {

        const response = await fetch(global.config.api_endpoints.tech);
        const res = await response.json();

        this.setState({
            technologies: res.response.data
        });
    }

    render() {
        return (
            <section className="home-page">
                <section className="header">
                    <div className="container">
                        <div className="row justify-content-end">
                            <div className="col-md-6">
                                <h1>LEARN PROGRAMMING</h1>
                                <p>It is a long established fact that a reader will be distracted by the readable
                                    content of
                                    a page when looking at its layout</p>
                                <div>
                                    <button className="btn btn-lg bg-main-color text-white mr-2">App Store</button>
                                    <button className="btn btn-lg bg-outline-main-color">Play Store</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="technologies">
                    <div className="row">
                        <React.Fragment>
                            <div className="col-md-4">
                                <div className="col-one">
                                    {this.state.technologies && this.state.technologies.map((tech, i) => (
                                        i % 3 === 0 && <TechCard key={i} tech={tech}/>
                                    ))}
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="col-two">
                                    {this.state.technologies && this.state.technologies.map((tech, i) => (
                                        i % 3 === 1 && <TechCard key={i} tech={tech}/>
                                    ))}
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="col-three">
                                    {this.state.technologies && this.state.technologies.map((tech, i) => (
                                        i % 3 === 2 && <TechCard key={i} tech={tech}/>
                                    ))}
                                </div>
                            </div>
                        </React.Fragment>

                    </div>
                </section>
            </section>
        );
    }

}

export default HomePage;