import HomePage from "./Home/HomePage";
import TechAboutPage from "./TechAbout/TechAboutPage";
import Error404 from "./errors/404";
import RegisterPage from "./Register/RegisterPage";


export default {
    HomePage: HomePage,
    TechAboutPage: TechAboutPage,
    RegisterPage: RegisterPage,
    Errors: {
        E404: Error404
    }
}