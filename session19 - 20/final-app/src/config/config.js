module.exports = global.config = {
    app_name : 'LMS V1',
    base_url : 'https://learning-test.ide-academy.org/api',
    media_url : 'https://learning-test.ide-academy.org/storage/',
    app_env : 'dev',
    api_endpoints: {
        tech: 'https://learning-test.ide-academy.org/api/techs',
        register: 'https://learning-test.ide-academy.org/api/register',
    }
};