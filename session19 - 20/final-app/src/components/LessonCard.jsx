import React from 'react';

const LessonCard = ({lesson}) => (
    <div className="col-6 d-flex mb-3">
        <div className="lesson-img">
            <img src="" alt="" className="w-100 h-100"/>
        </div>
        <div className="lesson-tittle">
            {lesson.english_title}
        </div>
    </div>
);

export default LessonCard;