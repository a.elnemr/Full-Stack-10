import React from 'react';
import {Link} from "react-router-dom";

const TechCard = ({tech}) => (
    <div className="tech-card card">
        <div className="card-img">
            <img src={tech.tech_logo} className="card-img-top"/>
        </div>
        <div className="card-body">
            <h5 className="card-title">{tech.name}</h5>
            <p className="card-text">{tech.description}</p>
            <Link to={`tech/${tech.id}`} className="btn btn-info">LEARN</Link>
        </div>
    </div>
);

export default TechCard;