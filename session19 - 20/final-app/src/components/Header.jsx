import React from 'react';
import {Link} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCommentDots} from "@fortawesome/free-solid-svg-icons";

const Header = () => (
    <React.Fragment>
        <section>
            <div className="container">
                <ul className="tob-bar mt-5 d-flex justify-content-end">
                    <li className="border-right"><Link to="/register" className="align-middle">Register</Link></li>
                    <li>
                        <Link to='contact-us' className="contact-us btn btn-primary">
                            <FontAwesomeIcon icon={faCommentDots}/> Contact us
                        </Link>
                    </li>
                </ul>
            </div>
        </section>
        <nav className="navbar navbar-expand-lg">
            <div className="container">
                <div className="collapse navbar-collapse justify-content-center">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/" className="nav-link active">Home</Link>
                        </li>
                        <li className="nav-item"><Link to="#" className="nav-link">Start Learn</Link></li>
                        <li className="nav-item"><Link to="#" className="nav-link">Career Path</Link></li>
                        <li className="nav-item"><Link to="#" className="nav-link">Books</Link></li>
                        <li className="nav-item"><Link to="#" className="nav-link">Contact</Link></li>
                    </ul>
                </div>
            </div>
        </nav>
    </React.Fragment>
);

export default Header;