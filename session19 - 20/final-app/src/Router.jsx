import React from 'react';
import Pages from "./pages";
import {Route, Switch} from "react-router-dom";

const Router = () => (
    <Switch>
        <Route path='/' component={Pages.HomePage} exact/>
        <Route path='/tech/:id' component={Pages.TechAboutPage} exact/>
        <Route path='/register' component={Pages.RegisterPage} exact/>
        <Route path='/404' component={Pages.Errors.E404} exact/>

        <Route path='*' component={Pages.Errors.E404}/>
    </Switch>
);

export default Router;