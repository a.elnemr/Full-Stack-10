(() => {
    "use strict";
    const baseUrl = 'https://jsonplaceholder.typicode.com';

    fetch( `${baseUrl}/posts`)
        .then(response => {
            return response.json()
        })
        .then(data => {
            const postsList = document.getElementById('posts-list');

            let tempPosts = '';

            data.map(post => {

                /*fetch(`https://jsonplaceholder.typicode.com/users/${post.userId}`)
                    .then(res => res.json())
                    .then(user => {

                    });*/

                tempPosts += `
                    <div class="post-preview">
                        <a href="post.html">
                            <h2 class="post-title">
                                ${post.title}
                            </h2>
                            <h3 class="post-subtitle">
                                ${post.body.substring(0, 60)}
                                ${post.body.length > 60 && '...'}
                            </h3>
                        </a>
                        <p class="post-meta">
                            Posted by
                            <a href="#">User Name</a>
                            on September 24, 2019
                        </p>
                    </div>
                    <hr>
                `;
            });

            tempPosts += `
                <!-- Pager -->
                <div class="clearfix">
                  <a class="btn btn-primary float-right" href="#">Older Posts &rarr;</a>
                </div>
            `;

            postsList.innerHTML = tempPosts;
        });
})();