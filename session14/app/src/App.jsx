import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Search from "./components/Search";
import HomePage from "./components/HomePage";
import 'bootstrap/dist/css/bootstrap.min.css'
import "./App.css";

const App = () => (
    <div className="container">
        <Header/>
        <h1>App</h1>
        <Search/>
        <HomePage/>
        <Footer/>
    </div>
);

export default App;