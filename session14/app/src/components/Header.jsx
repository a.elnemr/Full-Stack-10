import React from 'react';

const Header = () => (
    <header>
       <nav>
           <ul className="nav">
               <li className="nav-item">
                   <a className="nav-link active" href="#">Active</a>
               </li>
               <li className="nav-item">
                   <a className="nav-link" href="#">Link</a>
               </li>
               <li className="nav-item">
                   <a className="nav-link" href="#">Link</a>
               </li>
               <li className="nav-item">
                   <a className="nav-link disabled" href="#" tabIndex="-1" aria-disabled="true">Disabled</a>
               </li>
           </ul>
       </nav>
    </header>
);

export default Header;