import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// const {App, About} = Component;
// const arr = {App: 'App', About: 'About'};
// const About = arr.About;
// const App = arr.App;

// const {About, App} = arr;

// console.log(App);
//
ReactDOM.render(
    <App/>,
    document.getElementById('root')
);


// document.getElementById('root').innerHTML = `<h1>React <br> JS</h1>`;