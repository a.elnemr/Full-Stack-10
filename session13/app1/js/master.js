const menuLi = 'aside ul li';
$(menuLi).click(function () {

    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).children('p').hide(500);
        return;
    }
    // reset
    $(menuLi).removeClass('active');
    $(menuLi).children('p').hide(500);
    /*$(menuLi).children('p').css('display', 'none');*/

    // set current active
    $(this).addClass('active');
    $(this).children('p').show(500);
    // $(this).children('p').show(50000);
    /*$(this).children('p').css('display', 'block');*/
});

$('.btn-login').click(function () {
    $('.login-form').animate({
        top: "100",
        height: "show"
    }, 500);
});

$('.btn-login-form-close').click(function () {
    // $('.login-form').hide(500);
    $('.login-form').animate({
        top: "0",
        height: "hide"
    }, 500);
});