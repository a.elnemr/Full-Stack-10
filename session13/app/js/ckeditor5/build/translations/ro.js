(function(d){d['ro']=Object.assign(d['ro']||{},{a:"Nu se poate încărca fișierul:",b:"Bară imagine",c:"Bară tabel",d:"Bloc citat",e:"Inserează imagine sau fișier",f:"Alege titlu",g:"Titlu",h:"Mărește indent",i:"Micșorează indent",j:"Link",k:"Listă numerotată",l:"Listă cu puncte",m:"widget media",n:"Inserează media",o:"URL-ul nu trebuie să fie gol.",p:"Acest URL media nu este suportat.",q:"Inserează tabel",r:"Antet coloană",s:"Inserează coloană la stânga",t:"Inserează coloană la dreapta",u:"Șterge coloană",v:"Coloană",w:"Rând antet",x:"Inserează rând dedesubt",y:"Inserează rând deasupra",z:"Șterge rând",aa:"Rând",ab:"Îmbină celula în sus",ac:"Îmbină celula la dreapta",ad:"Îmbină celula în jos",ae:"Îmbină celula la stânga",af:"Scindează celula pe verticală",ag:"Scindează celula pe orizontală",ah:"Îmbină celulele",ai:"Îngroșat",aj:"Cursiv",ak:"widget imagine",al:"Introdu titlul descriptiv al imaginii",am:"Imagine mărime completă",an:"Imagine laterală",ao:"Imagine aliniată la stânga",ap:"Imagine aliniată pe centru",aq:"Imagine aliniată la dreapta",ar:"Inserează imagine",as:"Încărcare eșuată",at:"Încărcare în curs",au:"Bară widget",av:"Nu se poate obtine URL-ul imaginii redimensionate.",aw:"Selecția imaginii redimensionate eșuată",ax:"Nu se poate insera imaginea la poziția curentă.",ay:"Inserție imagine eșuată",az:"Paragraf",ba:"Titlu 1",bb:"Titlu 2",bc:"Titlu 3",bd:"Titlu 4",be:"Titlu 5",bf:"Titlu 6",bg:"Schimbă textul alternativ al imaginii",bh:"Bară listă opțiuni",bi:"Editor de text, %0",bj:"Anulare",bk:"Revenire",bl:"Salvare",bm:"Anulare",bn:"Text alternativ",bo:"%0 din %1",bp:"Înapoi",bq:"Înainte",br:"Bară editor",bs:"Show more items",bt:"Deschide în tab nou",bu:"Descărcabil",bv:"Adaugă URL-ul media in input.",bw:"Sugestie: adaugă URL-ul în conținut pentru a fi adăugat mai rapid.",bx:"Media URL",by:"Șterge link",bz:"Modifică link",ca:"Deschide link în tab nou",cb:"Acest link nu are niciun URL",cc:"Link URL"})})(window.CKEDITOR_TRANSLATIONS||(window.CKEDITOR_TRANSLATIONS={}));