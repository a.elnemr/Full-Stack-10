import React from 'react';
import {useParams} from 'react-router-dom';
import TasksList from "../components/TasksList";


const TaskListView = props => {
    const {id} = useParams(),
        project = props.getProjectById(id)
    ;

    return (
        <React.Fragment>
            <h1>{project.title}: Tasks Management</h1>
            <div className="add-new form-group text-center">
                <input type="text"
                       className="form-control m-2"/>
                <button className="btn btn-success btn-block m-2"
                        onClick={props.onClickNewProjectHandle}>
                    Add New
                </button>
            </div>
            <TasksList deleteTaskFromProject={props.deleteTaskFromProject}
                       projectId={project.id}
                       tasks={project.tasks} />
        </React.Fragment>
    )
};


export default TaskListView;