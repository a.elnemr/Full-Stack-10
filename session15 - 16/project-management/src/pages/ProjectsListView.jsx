import React from 'react';
import ProjectList from "../components/ProjectList";


const ProjectsListView = props => {
    return (
        <React.Fragment>
            <h1>Project Management</h1>
            <div className="add-new form-group text-center">
                <input type="text"
                       value={props.newProject}
                       onChange={props.changeHandle}
                       className="form-control m-2"/>
                <button className="btn btn-success btn-block m-2"
                        onClick={props.onClickNewProjectHandle}>
                    Add New
                </button>
            </div>
            <ProjectList projects={props.projects}
                         deleteProject={props.onClickDeleteProjectHandle}/>
        </React.Fragment>
    )
};


export default ProjectsListView;