import React from 'react';


export class TasksListItem extends React.Component {

    state = {
        editable: false,
        taskTitle: ''
    };

    onClickEdit = () => {
        this.setState({
            editable: true,
            taskTitle: this.props.task.title
        });
    };

    onClickSave = () => {
        const task = this.props.task;
        task.title = this.state.taskTitle;

        this.setState({
            editable: false,
            taskTitle: ''
        });
    };

    onChangeHandler = e => {
        const taskTitle = e.target.value;
        this.setState({
            taskTitle
        })
    };

    render() {
        return (
            <li className="list-group-item">
                {this.state.editable ? <input type="text"
                                              onChange={this.onChangeHandler}
                                              value={this.state.taskTitle}/>
                    : this.props.task.title}

                {this.state.editable ? (
                        <button className="btn btn-primary mr-2 float-right"
                                onClick={this.onClickSave}
                        >
                            Save
                        </button>
                    )
                    : (
                        <button className="btn btn-info mr-2 float-right"
                                onClick={this.onClickEdit}
                        >
                            Edit
                        </button>
                    )}

                <button className="btn btn-danger mr-2 float-right"
                        onClick={() => this.props
                            .deleteTaskFromProject(this.props.projectId, this.props.task)}
                >
                    Delete
                </button>
            </li>
        );
    }
}


export default TasksListItem;