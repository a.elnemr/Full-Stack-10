import React from 'react';
import {NavLink} from "react-router-dom";


export class ProjectListItem extends React.Component {

    state = {
        project: '',
        editable: false
    };

    onClickEditButton = () => {
        this.setState({
            editable: true,
            project: this.props.project.title
        });
    };

    onClickSaveButton = () => {
        const updatedProject = this.props.project;
        updatedProject.title = this.state.project;

        this.setState({
            editable: false,
            project: ''
        });
    };

    changeHandle = e => {
        const project = e.target.value;
        // this.state.newProject = newProject;
        this.setState({
            project
        });
    };

    render() {
        return (
            <li className="list-group-item">
                {
                    this.state.editable ? <input type="text"
                                                 onChange={this.changeHandle}
                                                 value={this.state.project}/>
                        :
                        <NavLink to={`/tasks/${this.props.project.id}`} className="btn mr-2">
                            {this.props.project.title}
                        </NavLink>
                }

                {this.state.editable ? (
                        <button className="btn btn-primary mr-2 float-right"
                                onClick={this.onClickSaveButton}>
                            Save
                        </button>
                    )
                    : (
                        <button className="btn btn-info mr-2 float-right"
                                onClick={this.onClickEditButton}>
                            Edit
                        </button>
                    )}

                <button className="btn btn-danger mr-2 float-right"
                        onClick={() => this.props.deleteProject(this.props.project)}>
                    Delete
                </button>
            </li>
        );
    }
}


export default ProjectListItem;