import React from 'react';
import ProjectListItem from './ProjectListItem';


export const ProjectList = props => (
    <ul className="list-group">
        {props.projects && props.projects.map(project => (
                <ProjectListItem key={project.id}
                                 project={project}
                                 deleteProject={props.deleteProject}/>
            )
        )}
    </ul>
);

export default ProjectList;