import React from 'react';
import TasksListItem from "./TasksListItem";


export const TasksList = props => (
    <ul className="list-group">
        {props.tasks ? (props.tasks
                .map(task => <TasksListItem key={task.id}
                                            projectId={props.projectId}
                                            deleteTaskFromProject={props.deleteTaskFromProject}
                                            task={task}/>))
            : (<p className="alert alert-danger text-center">No Tasks</p>)}
    </ul>
);

export default TasksList;