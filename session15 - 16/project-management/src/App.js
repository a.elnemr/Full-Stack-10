import React, {Component} from 'react';
import ProjectsListView from "./pages/ProjectsListView";
import TaskListView from "./pages/TasksListView";
import Navbar from "./Layouts/Navbar";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';


class App extends Component {

    state = {
        projects: [
            {
                id: 1,
                title: 'LMS',
                tasks: [
                    {
                        id: 1,
                        title: 'Init project',
                        isDone: false
                    },
                    {
                        id: 2,
                        title: 'Front-End',
                        isDone: false
                    },
                ]
            },
            {
                id: 2,
                title: 'CMS',
            },
            {
                id: 3,
                title: 'CRM',
            },
            {
                id: 4,
                title: 'Database',
            },
        ],
        newProject: ''
    };

    changeHandle = e => {
        const newProject = e.target.value;
        // this.state.newProject = newProject;
        this.setState({
            newProject
        });
    };

    onClickNewProjectHandle = () => {
        let projects = this.state.projects;
        // ES5
        /*projects.push({
            id: projects.length + 1,
            title: this.state.newProject
        });*/
        // ES6
        const newProject = {
            id: projects.length + 1,
            title: this.state.newProject,
            tasks: []
        };
        projects = [...projects, newProject];

        this.setState({
            projects,
            newProject: ''
        });

    };

    onClickDeleteProjectHandle = project => {
        let projects = this.state
            .projects
            .filter(rowProject => project !== rowProject);
        this.setState({
            projects
        });
    };

    // Tasks Functions
    getProjectById = projectId => {
        const project = this.state
            .projects
            .filter(project => projectId == project.id);

        if (project.length) {
            return project[0];
        } else {
            return false
        }

    };

    deleteTaskFromProject = (projectId, task) => {
        const project = this.getProjectById(projectId);
        if (project && project.tasks) {
            project.tasks = project.tasks
                .filter(rowTask => task != rowTask);
        }

        /*const projects = this.state.projects.map(rowProject => {
            if (rowProject == project) {
                return project;
            }
            return rowProject;
        });*/

        const projects = this.state.projects;

        this.setState({
            projects
        });

    };

    render() {
        return (
            <div className="App">
                <Router>
                    <Navbar/>
                    <div className="container">
                        <Switch>
                            <Route exact path="/">
                                <ProjectsListView projects={this.state.projects}
                                                  newProject={this.state.newProject}
                                                  changeHandle={this.changeHandle}
                                                  onClickNewProjectHandle={this.onClickNewProjectHandle}
                                                  onClickDeleteProjectHandle={this.onClickDeleteProjectHandle}/>
                            </Route>
                            <Route exact path="/tasks/:id">
                                <TaskListView
                                    deleteTaskFromProject={this.deleteTaskFromProject}
                                    getProjectById={this.getProjectById}/>
                            </Route>
                            <Route exact path="/about">
                                <h1>About</h1>
                            </Route>
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    }

}

export default App;
